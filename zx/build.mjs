#!/usr/bin/env zx
$.shell = '/usr/bin/zsh'
const fs = require('fs');

let url = "https://github.com/jarun/nnn"
let bin = "nnn"
let dir = `/tmp/nnn`

// check if directory exists
if (fs.existsSync(dir)) {
  // delete directory recursively
  fs.rm(dir, { recursive: true }, (err) => {
    if (err) {
      throw err;
    }
    console.log(`${dir} is deleted!`);
  });
} else {
  console.log('Directory not found.');
}

let cmd = `echo test`
console.log(chalk.blue(cmd))

try {
  await $`'${cmd}'`
} catch (p) {
  console.log(`Exit code: ${p.exitCode}`)
  console.log(`Error: ${p.stderr}`)
}
