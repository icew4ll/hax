#!/usr/bin/env zx
let count = parseInt(await $ `ls -1 | wc -l`);
console.log(chalk.blue(`Files count: ${count}`));
export {};
