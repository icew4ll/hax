#!/usr/bin/env zx

let count = parseInt(await $`ls -1 | wc -l`)
console.log(chalk.blue(`Files count: ${count}`))

// parallel execute
let hosts = ["2", "2"]
await Promise.all(hosts.map(host =>
  $`sleep ${host} && echo "done"`  
))

// If the executed program returns a non-zero exit code, ProcessOutput will be thrown.
try {
  await $`exit 1`
} catch (p) {
  console.log(`Exit code: ${p.exitCode}`)
  console.log(`Error: ${p.stderr}`)
}

 // redirect to stdout
await $`cat mark.md`.pipe(process.stdout)

// change dir
cd('/tmp')
await $`pwd` // outputs /tmp

// fetch
let resp = await fetch('http://wttr.in')
if (resp.ok) {
  console.log(await resp.text())
}

// readline
let bear = await question('What kind of bear is best? ')
console.log(chalk.blue(`bear: ${bear}`))
let token = await question('Choose env variable: ', {
  choices: Object.keys(process.env)
})
console.log(chalk.blue(`token: ${token}`))
