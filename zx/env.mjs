#!/usr/bin/env zx

let token = await question('Choose env variable: ', {
  choices: Object.keys(process.env)
})
console.log(chalk.blue(`token: ${token}`))
