# https://mirrors.edge.kernel.org/pub/software/scm/git/git-1.8.3.2.tar.gz
use utils
var url = https://mirrors.edge.kernel.org/pub/software/scm/git
var work = /tmp/git
var latest = (utils:get_links $url | rg "tar.gz" | rg "git-[0-9]{1,2}.[0-9]{2}" | tail -1)

# install deps
# sudo apt-get install dh-autoreconf libcurl4-gnutls-dev libexpat1-dev gettext libz-dev libssl-dev asciidoc xmlto docbook2x

# clean and download
utils:green "START"
utils:clean_dir $work
utils:down_targz $url/$latest $work

# build
cd git*
make configure
./configure --prefix=/usr
make all doc info
sudo make install install-doc install-html install-info

# utils:linky $work/git*/git ~/bin/git
utils:green "COMPLETE"
