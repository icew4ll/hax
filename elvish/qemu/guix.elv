var h = [
&vm=~/vm
&iso=~/vm/iso
&img=~/vm/img
]
var g = [
&img=$h[img]"/guix.img"
# &iso=$h[iso]"/nonguix-2022-01-04.iso"
&iso=$h[iso]"/nonguix-2022-01-25.iso"
&bios="/gnu/store/h85g6g7cllis5g4fdj2swqcnpgnzx6sd-ovmf-20170116-1.13a50a6/share/firmware/ovmf_x64.bin"
&size=50G
&ram=4096
]
qemu-system-x86_64 -bios $g[bios] -nic user,model=virtio-net-pci,hostfwd=tcp::10022-:22 -vga virtio -smp 1 -enable-kvm -boot menu=on,order=d -drive file=$g[img] -drive media=cdrom,file=$g[iso] -m $g[ram] 
# qemu-system-x86_64 -bios $g[bios] -nic user,model=virtio-net-pci,hostfwd=tcp::10022-:22 -vga virtio -smp 1 -enable-kvm -boot menu=on,order=d -drive file=$g[img] -m $g[ram] 
