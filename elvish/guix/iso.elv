var h = [
&chn=~/m/guix/main/channels
&iso=~/m/guix/main/iso
&out=~/vm/iso
]
var f = [
&base=$h[chn]"/base-channels.scm"
&main=$h[iso]"/channels.scm"
&man=$h[iso]"/beta.scm"
]

# Write out the channels file so it can be included
# $f[base] is defined by https://gitlab.com/nonguix/nonguix
# guix time-machine -C $f[base] -- describe -f channels | tee $f[main]

# edit manifest
# e $f[man]

# build image per manifest
guix time-machine -C $f[main] -- system image -t iso9660 $f[man]

# cp image
var tmp = (fd . /gnu/store -e iso)
var iso = $h[out]"/nonguix-"(date "+%Y-%m-%d")".iso"
# if iso exists rm
if ?(test -f $iso) {
  chmod 700 $iso
  rm $iso
}
cp $tmp $iso

# clean up iso
guix package --delete-generations
guix gc --collect-garbage

# print iso
ls $iso
