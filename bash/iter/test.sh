#!/bin/bash

INSTALL_PATH="/mnt/install"

arr=(
	@
	@home
	@root
	@usr
	@usr/local # exclude from rollback
	# @/opt      # exclude from rollback
)

for i in "${arr[@]}"; do
	echo "$INSTALL_PATH$(echo "$i" | sed "s/@/\//g")"
done
