#!/bin/bash

declare -A top_arr=(
	["sys"]="/"
	["home"]="/home"
	["opt"]="/opt"
	["root"]="/root"
	["srv"]="/srv"
	["usr_local"]="/usr/local"
	["var_lib_containers"]="/var/lib/containers"
	["var_lib_libvirt_images"]="/var/lib/libvirt/images"
)
for key in "${!top_arr[@]}"; do
	val=${top_arr[$key]}
	echo "$key $val"
done

# dir=/mnt/btrfs-root
# for i in {tmp,spool,log};
# do btrfs subvolume create @var/$i;
# done
# btrfs subvolume list $dir
# btrfs subvolume show $dir/@var/log
