#!/bin/bash

arr=(
@
@home
@root
@srv
@opt
@usr
@usr/local 
@boot
@var/tmp
@var/spool
@var/cache
@var/games
@var/www
@var/lib
@var/lib/libvirt
@var/lib/docker
@var/lib/NetworkManager
)

for i in "${arr[@]}"; do
	echo "$i"
done
