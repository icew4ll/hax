#!/bin/bash

arr=(
	@
	@home
	@root
	@usr
	@usr/local # exclude from rollback
	@/srv      # exclude from rollback
	@/opt      # exclude from rollback
	@var
	# exclude from rollback
	@var/tmp
	@var/spool
	@var/log
	@var/log/journal
	@var/cache
	@var/crash
	@var/games
	@var/www
	@var/lib
	@var/lib/libvirt
	@var/lib/docker
	@var/lib/machines
	@var/lib/containers
	@var/lib/AccountsService
	@var/lib/NetworkManager
)

for i in "${arr[@]}"; do
	if [ ! -e "/mnt/btrfs-root$i" ]; then
		echo btrfs subvolume create "/mnt/btrfs-root/$i"
		btrfs subvolume create "/mnt/btrfs-root/$i"
	fi
done
