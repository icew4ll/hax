#!/bin/bash
source env.sh
# opts="-av --info=progress2 --delete"
opts="-avP"
# rsync --info=help
[ ! -d "$dest" ] && sudo mkdir "$dest"
arr=(
	~/Documents
	~/Downloads
	~/Pictures
	~/Music
	~/Videos
	~/data
	~/bak
	# ~/vm
)
for src in "${arr[@]}"; do
	[ -d "$src" ] && echo rsync "$opts" "$src" "$dest"
	[ -d "$src" ] && sudo rsync "$opts" "$src" "$dest"
done
