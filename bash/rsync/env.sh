#!/bin/bash
dev="/dev/sda"
mnt="/mnt/ark"
dest=$mnt/home

green() {
	echo -e "\e[0;32m$1\e[0m"
}

yellow() {
	echo -e "\e[1;33m$1\e[0m"
}

red() {
	echo -e "\e[1;31m$1\e[0m"
}
