#!/bin/bash
source env.sh

if grep "$mnt" /proc/mounts > /dev/null 2>&1; then
	red "$mnt already mounted"
else
	yellow "mounting $mnt ..."
	opts="rw,noatime,compress=zstd,compress-force,space_cache=v2,commit=120,noautodefrag"

	# create virtual block device
	UUID=$(sudo cryptsetup luksUUID "${dev}1")
	sudo cryptsetup luksOpen "${dev}1" "$UUID"
	sudo cryptsetup -v status "$UUID"
	map=$(sudo cryptsetup -v status "$UUID" | grep mapper | cut -d" " -f1)

	# mount
	[ ! -d "$mnt" ] && sudo mkdir "$mnt"
	sudo mount -o "$opts" "$map" "$mnt"
	grep "$mnt" /proc/mounts > /dev/null 2>&1 && green "mounted $mnt"
fi
