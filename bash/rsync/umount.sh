#!/bin/bash

source env.sh
opts="remount,ro,noatime,compress=zstd,compress-force,space_cache=v2,commit=120,noautodefrag"

# close luks device
if grep "$mnt" /proc/mounts > /dev/null 2>&1; then
	while grep "$mnt" /proc/mounts | grep rw > /dev/null 2>&1; do
		yellow "$mnt is read write"
		if UUID=$(sudo cryptsetup luksUUID "${dev}1"); then
			map=$(sudo cryptsetup -v status "$UUID" | grep mapper | cut -d" " -f1)
			green "LUKS UUID for ${dev}1: $UUID"
			# remounting ro is guaranteed to fail if files open on device
			yellow "$mnt remounting as ro..."
			sudo mount -o "$opts" "$map" "$mnt"
		else
			# exit condition
			red "${dev}1 is not LUKS"
			break
		fi
		sleep 1
	done
	yellow "$mnt unmounting..."
	sudo umount "$mnt"
	yellow "Closing LUKS $UUID ..."
	sudo cryptsetup -v luksClose "$UUID"
	green "drive safe to remove"
else
	red "not mounted"
fi
