#!/bin/bash
# config uses default bridge br0
img=~/vm/img/linux.qcow2
iso=~/vm/iso/archlinux-2022.02.01-x86_64.iso
bios="/gnu/store/h85g6g7cllis5g4fdj2swqcnpgnzx6sd-ovmf-20170116-1.13a50a6/share/firmware/ovmf_x64.bin"
size=25G
ram=8192
# ram=4096
# -smp cores=4,threads=1 \
# working:
# -net nic -net bridge,br="$br" \
# -device virtio-net-pci,netdev=hn0,id=nic1 \
# br=br0
# -display spice-app \

# create qcow2
[ ! -f "$img" ] && qemu-img create -f qcow2 "$img" "$size"

# create vm
vm() {
	qemu-system-x86_64 \
		-bios "$bios" \
		-enable-kvm \
		-cpu host \
		-soundhw all \
		-smp cores=4,threads=1 \
		-vga qxl \
		-device virtio-serial-pci \
		-spice unix=on,addr=/tmp/vm_spice.socket,disable-ticketing=on \
		-device virtserialport,chardev=spicechannel0,name=com.redhat.spice.0 \
		-chardev spicevmc,id=spicechannel0,name=vdagent \
		-nic user,model=virtio-net-pci,hostfwd=tcp::10022-:22 \
		-boot menu=on,order=d \
		-drive file="$img",if=virtio \
		-drive media=cdrom,file="$iso" \
		-daemonize \
		-m "$ram"
}

if pgrep qemu; then
  echo on
	pkill qemu
fi
vm
remote-viewer spice+unix:///tmp/vm_spice.socket

# https://www.dedoimedo.com/computers/qemu-virgil.html
# https://www.collabora.com/news-and-blog/blog/2021/11/26/venus-on-qemu-enabling-new-virtual-vulkan-driver
# https://archived.forum.manjaro.org/t/the-struggle-to-get-any-3d-acceleration-in-qemu-kvm-please-help/93174
