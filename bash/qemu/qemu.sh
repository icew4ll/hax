#!/bin/bash
img="$HOME/vm/img/IE11 - Win81.qcow2"
ram=2048
qemu-system-x86_64 \
	-vga virtio \
	-smp 1 \
	-enable-kvm \
	-nic user,model=virtio-net-pci \
	-boot menu=on,order=d \
	-drive file="$img" \
	-m "$ram"
