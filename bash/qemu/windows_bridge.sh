#!/bin/bash
# worked after reboot, network issue?
# after running adds tap0
# bridge network allows vm access to local network
# virbr0 virt-manager virtual bridge (virtual switch)
# vnet0 virtual nic
# -net nic \
# -net bridge,br=br0 \
# -net bridge,br=$br \
# img="$HOME/vm/img/IE11 - Win81.qcow2"
# -netdev bridge,br=br0,id=net0 \
img="/home/ice/vm/img/IE11 - Win81.qcow2"
ram=8192
tap=tap0
br=br0
qemu-system-x86_64 \
	-enable-kvm \
	-cpu host \
	-smp cores=4,threads=1 \
	-machine type=pc,accel=kvm \
	-vga virtio \
  -net nic -net bridge,br=br0 \
	-boot menu=on,order=d \
	-drive file="$img" \
	-m "$ram"
