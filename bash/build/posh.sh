#!/bin/bash

url=https://github.com/JanDeDobbeleer/oh-my-posh/releases/latest/download/posh-linux-amd64
sudo wget $url -O /usr/local/bin/oh-my-posh
sudo chmod +x /usr/local/bin/oh-my-posh

url=https://github.com/JanDeDobbeleer/oh-my-posh/releases/latest/download/themes.zip
mkdir ~/.poshthemes
wget $url -O ~/.poshthemes/themes.zip
unzip ~/.poshthemes/themes.zip -d ~/.poshthemes
chmod u+rw ~/.poshthemes/*.json
rm ~/.poshthemes/themes.zip
