#!/bin/bash

source utils.sh

repo="crystal-lang/crystal"
url=$(git_release $repo)
tag="linux-x86_64.tar.gz"
in="bin/crystal"
base=$(basename "$url")
dl=$(git_tag_latest "$url" "$tag")
tar_build "$dl"
linky "$build"/"$base"*/"$in" "$bin"/"$base"
green "Complete"
