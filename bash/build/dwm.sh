#!/bin/bash
source utils.sh

# sudo xbps-install -Sy git
url=https://git.suckless.org/dwm
base=$(basename $url)
dir=/tmp/"$base"
clean_dir "$dir"
git clone "$url" "$dir"
ls "$dir"
