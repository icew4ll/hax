#!/bin/bash

source utils.sh

green "START"
repo="arkenfox/user.js"
base="arkenfox"
url=$(git_release $repo)
tag="tarball_url"
dest="$build"/"$base"
# dl=$(git_down "$url")
dl=$(git_tag "$url" "$tag")
clean_dir "$dest"
extract_gz "$dl" "$dest" 
green "Complete"
