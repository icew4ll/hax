#!/bin/bash

source utils.sh
url=https://beta.quicklisp.org/quicklisp.lisp
key=https://beta.quicklisp.org/release-key.txt
pub=https://beta.quicklisp.org/quicklisp.lisp.asc
match="D7A3 489D DEFE 32B7 D0E7 CC61 3079 65AB 028B 5FF7"
dir=/tmp/build

clean_dir $dir
wget "$url"
wget "$key"
wget "$pub"

# verify dl
cd $dir || exit
gpg --import release-key.txt > /dev/null 2>&1
# This will take n as the value of n and loop through that number through the last field NF, for each iteration it will print the current value, if that is not the last value in the line it will print OFS after it (space), if it is the last value on the line it will print ORS after it (newline).
cert=$(gpg --verify "$(basename $pub)" "$(basename $url)" |& grep Primary | awk -v n=4 '{ for (i=n; i<=NF; i++) printf "%s%s", $i, (i<NF ? OFS : ORS)}')

green_b "$cert"
green_b "$match"
if [[ "$cert" == "$match" ]]; then
  green MATCH
else
  green "no match" && exit
fi

# install
sbcl --load quicklisp.lisp
sbcl --load quicklisp.lisp --eval "(quicklisp-quickstart:install)"

# (quicklisp-quickstart:install)
# (ql:add-to-init-file)
# (ql:quickload "clx")
# (ql:quickload "cl-ppcre")
# (ql:quickload "alexandria")
(ql:quickload :stumpwm)
(ql:quickload :xembed)
(ql:quickload :clx-truetype)
(ql:quickload :swank)
(ql:quickload :quicklisp-slime-helper)
(ql:quickload :zpng)
# (exit)
