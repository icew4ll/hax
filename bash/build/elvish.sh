#!/bin/bash

source utils.sh

green "START"
repo="elves/elvish"
url="https://github.com/$repo"
base=$(basename "$repo")
dest="$build/$base"
cloner "$url" "$dest"
make get
green "Complete"
