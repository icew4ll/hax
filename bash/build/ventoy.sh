#!/bin/bash

source utils.sh

# https://github.com/ventoy/Ventoy/releases
repo="ventoy/Ventoy"
url=$(git_release "$repo")
tag="linux.tar.gz"
dl=$(git_tag_latest "$url" "$tag")

green Latest
echo "$dl"

green Latest
dest=/tmp/$(basename "$repo")
# dest="$build"/$(basename "$repo")
clean_dir "$dest"
extract_gz "$dl" "$dest"

# usage
cd "$dest"/ventoy* || exit
scr=Ventoy2Disk.sh
dev=/dev/sda
sudo sh "$scr" -i "$dev"
