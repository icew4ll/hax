#!/bin/bash

# sample cannot exceed 20 sec
url="https://api.audd.io/"
file="/tmp/violin_small.m4a.m4a"
curl "$url" \
	-F api_token='test' \
	-F file=@"$file" \
	-F return='apple_music,spotify'
