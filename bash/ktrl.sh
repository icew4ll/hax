#!/bin/bash
dev=/dev/input/by-id
kb=usb-SEMITEK_USB-HID_Gaming_Keyboard_SN0000000001-event-kbd
file=/etc/systemd/system/ktrl.service

# service
sudo cp /home/ice/.cargo/bin/ktrl /usr/local/bin
sudo rm $file
sudo tee -a $file << EOF
[Unit]
Description=ktrl

[Service]
User=ktrl
Environment=HOME=/opt/ktrl
ExecStart=/usr/local/bin/ktrl -d $dev/$kb
Restart=on-failure

[Install]
WantedBy=multi-user.target
EOF

# config
file=/opt/ktrl/cfg.ron
sudo tee $file << EOF
(
    // ktrl will register a TapHold as an hold after 300ms
    tap_hold_wait_time: 111,

    // ktrl will register a TapDance if all taps occur within a 1000ms period (1s)
    tap_dance_wait_time: 1000,

    layer_aliases: {
        "base": 0,
    },

    layer_profiles: {
    },

    layers: [
        {
            KEY_CAPSLOCK:  TapHold(Key(KEY_ESC), Key(KEY_LEFTCTRL)),
        },
    ],
)
EOF

sudo systemctl daemon-reload
sudo systemctl enable --now ktrl.service
sudo systemctl restart ktrl.service
journalctl -f -u ktrl.service --no-pager

# fix perms
# sudo cp /home/ice/.cargo/bin/ktrl /usr/local/bin
# sudo runuser -u ktrl -- /usr/local/bin/ktrl -d /dev/input/by-id/usb-SEMITEK_USB-HID_Gaming_Keyboard_SN0000000001-event-kbd
# la /home
# la /usr/local/bin
