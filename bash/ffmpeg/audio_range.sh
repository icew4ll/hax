#!/bin/bash
# 51:
sam="~/Music/movie/Fountain soundtrack - Death is the road to awe-7FDAkpQSJVA.opus"
out="/tmp/awe"
beg=00:00:00
end=00:19:56
ffmpeg \
	-i "$sam" \
	-ss "$beg" \
	-to "$end" \
	-c copy "$out".m4a
