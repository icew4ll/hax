#!/bin/bash
if which sk >/dev/null 2>&1; then
	entry=$(fd -t f 2>/dev/null | sk)
else
	exit 1
fi

case "$(file -biL "$entry")" in
	*text*)
		"${VISUAL:-$EDITOR}" "$entry"
		;;
	*)
		if uname | grep -q "Darwin"; then
			open "$entry" >/dev/null 2>&1
		else
			xdg-open "$entry" >/dev/null 2>&1
		fi
		;;
esac
