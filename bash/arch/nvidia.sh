#!/bin/bash
# https://github.com/korvahannu/arch-nvidia-drivers-installation-guide

# preqs
base_driver=nvidia
sudo pacman -Syu
sudo pacman -S base-devel linux-headers git
paru -S "$base_driver" nvidia-utils lib32-nvidia-utils nvidia-settings

# grub
file=/etc/default/grub
tag="GRUB_CMDLINE_LINUX_DEFAULT="
app="nvidia-drm.modeset=1"
lim="\""
fix="/$tag/s/$lim$/ $app\"/"
sed "$fix" "$file" | grep "$tag"
sudo sed -i "$fix" "$file"
sudo grub-mkconfig -o /boot/grub/grub.cfg

# mkinitcpio
file=/etc/mkinitcpio.conf
tag="^MODULES="
app="nvidia nvidia_modeset nvidia_uvm nvidia_drm"
lim=" )"
fix="/$tag/s/$lim$/ $app$lim/"
sed "$fix" "$file" | grep "$tag"
sudo sed -i "$fix" "$file"
sudo mkinitcpio -P

# nvidia hook
file=/etc/pacman.d/hooks/nvidia.hook
mkdir -p "$(dirname "$file")"
sudo tee "$file" <<-EOF
	[Trigger]
	Operation=Install
	Operation=Upgrade
	Operation=Remove
	Type=Package
	Target=$base_driver
	Target=linux
	# Change the linux part above and in the Exec line if a different kernel is used

	[Action]
	Description=Update Nvidia module in initcpio
	Depends=mkinitcpio
	When=PostTransaction
	NeedsTargets
	Exec=/bin/sh -c 'while read -r trg; do case $trg in linux) exit 0; esac; done; /usr/bin/mkinitcpio -P'
EOF
