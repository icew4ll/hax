#!/bin/bash

green() {
	echo -e "\e[0;32m$1\e[0m"
}

yellow() {
	echo -e "\e[1;33m$1\e[0m"
}

init() {
	yellow INIT
	# uncomment Color option
	file=/etc/pacman.conf
	opt=Color
	sed -i "/$opt/s/^#//g" "$file"

	# update mirrors
	systemctl start reflector

	# update time
	timedatectl set-ntp true
	MIRROR=/etc/pacman.d/mirrorlist
	cat "$MIRROR"

	# preqs
	pacman -Sy --needed --noconfirm cryptsetup btrfs-progs gdisk tree which glibc
}

partition() {
	yellow PARTITION
	# sda      8:0    0    50G  0 disk
	# ├─sda1   8:1    0     1G  0 part
	# ├─sda2   8:2    0    49G  0 part
	# └─sda5   8:5    0  1000K  0 part
	# clear partition table
	sgdisk --zap-all "$DISK"
	# create efi
	sgdisk -n1:1M:+1G -t1:EF00 "$DISK"
	# bios boot partition
	sgdisk -a1 -n5:24K:+1000K -t5:EF02 "$DISK"
	# main partition
	sgdisk -n2:0:0 "$DISK"
	lsblk -f
}

format() {
	yellow FORMAT
	while [ ! -e "$DISK-part2" ]; do
		sleep 2
		# Format and open LUKS container
		cryptsetup luksFormat --type luks1 "$DISK"-part2
		cryptsetup open "$DISK"-part2 "$BOOT_MAPPER_NAME"
		# format LUKS container as btrfs
		mkfs.btrfs "$BOOT_MAPPER_PATH"
		mount "$BOOT_MAPPER_PATH" "$INST_MNT"
		lsblk -f
    # sda
    # ├─sda1
    # │    vfat   FAT32 EFI       3DC0-AEE2
    # ├─sda2
    # │    crypto 1               edc1f051-e136-462d-8847-57bde6d626f5
    # │ └─cryptroot-luks1-partuuid-bd072cfb-7403-454e-a128-f4cde9789673
    # │    btrfs                  0d514f00-dae7-44d1-9ec0-56dfbc901358   48.5G     0% /tmp/tmp.YhVNYq3K4x
    # └─sda5
	done
}

subvols() {
	yellow SUBVOL
	cd "$INST_MNT" || exit

	btrfs subvolume create @
	mkdir @/0
	btrfs subvolume create @/0/snapshot

	for i in {home,root,srv,usr,usr/local,swap,var}; do
		btrfs subvolume create @"$i"
	done

	# exclude these dirs under /var from system snapshot
	for i in {tmp,spool,log}; do
		btrfs subvolume create @var/"$i"
	done

	btrfs subvolume list "$INST_MNT"
}

mount_sub() {
	yellow "MOUNT SUB"
	# mount main
	cd ~ || exit
	umount "$INST_MNT"
	mount "$BOOT_MAPPER_PATH" "$INST_MNT" -o subvol=/@/0/snapshot,"$BTRFS_OPTS"

	# create dirs
	mkdir -p "$INST_MNT"/{.snapshots,home,root,srv,tmp,usr/local,swap}
	mkdir -p "$INST_MNT"/var/{tmp,spool,log}

	# mount snaps
	mount "$BOOT_MAPPER_PATH" "$INST_MNT"/.snapshots/ -o subvol=@,"$BTRFS_OPTS"

	# mount subvolumes
	# separate /{home,root,srv,swap,usr/local} from root filesystem
	for i in {home,root,srv,swap,usr/local}; do
		mount "$BOOT_MAPPER_PATH" "$INST_MNT"/"$i" -o subvol=@"$i","$BTRFS_OPTS"
	done

	# separate /var/{tmp,spool,log} from root filesystem
	for i in {tmp,spool,log}; do
		mount "$BOOT_MAPPER_PATH" "$INST_MNT"/var/"$i" -o subvol=@var/"$i","$BTRFS_OPTS"
	done
	btrfs filesystem show
}

cow() {
	yellow "cow"
	for i in {swap,}; do
		chattr +C "$INST_MNT"/"$i"
	done
}

efi() {
	yellow "efi"
	mkfs.vfat -n EFI "$DISK"-part1
	mkdir -p "$INST_MNT"/boot/efi
	mount "$DISK"-part1 "$INST_MNT"/boot/efi
}

strap() {
	yellow "pacstrap"
	# INST_MNT=/tmp/tmp.TTBvj5VjAB
	pacstrap "$INST_MNT" base neovim mandoc grub cryptsetup btrfs-progs snapper snap-pac grub grub-btrfs openssh
	chmod 750 "$INST_MNT"/root
	chmod 1777 "$INST_MNT"/var/tmp/

	# system
	pacstrap "$INST_MNT" linux-zen linux-zen-headers
	pacstrap "$INST_MNT" linux-firmware dosfstools efibootmgr intel-ucode
}

fstab() {
	yellow "fstab"
	# genfstab (arch) fstabgen(artix)
	genfstab -U "$INST_MNT" >"$INST_MNT"/etc/fstab

	# remove hard-coded sys subvol
	sed -i "s|,subvolid=257,subvol=/@/0/snapshot||g" "$INST_MNT"/etc/fstab
}

key() {
	yellow "key"
	mkdir -p "$INST_MNT"/lukskey
	dd bs=512 count=8 if=/dev/urandom of="$INST_MNT"/lukskey/crypto_keyfile.bin
	chmod 600 "$INST_MNT"/lukskey/crypto_keyfile.bin
	yellow "Enter cryptsetup key:"
	cryptsetup luksAddKey "$DISK"-part2 "$INST_MNT"/lukskey/crypto_keyfile.bin
	chmod 700 "$INST_MNT"/lukskey
}

ramfs() {
	yellow "ramfs"
	mv "$INST_MNT"/etc/mkinitcpio.conf "$INST_MNT"/etc/mkinitcpio.conf.original
	tee "$INST_MNT"/etc/mkinitcpio.conf <<EOF
BINARIES=(/usr/bin/btrfs)
FILES=($CRYPTKEY)
HOOKS=(base udev autodetect modconf block encrypt filesystems keyboard fsck grub-btrfs-overlayfs)
EOF
}

grub() {
	yellow "grub"
	echo "GRUB_ENABLE_CRYPTODISK=y" >>"$INST_MNT"/etc/default/grub
	echo "GRUB_CMDLINE_LINUX=\"cryptdevice=PARTUUID=$BOOT_PARTUUID:$BOOT_MAPPER_NAME root=$BOOT_MAPPER_PATH CRYPTKEY=rootfs:$CRYPTKEY\"" >>"$INST_MNT"/etc/default/grub
}

swap() {
	yellow "swap"
	touch "$INST_MNT"/swap/swapfile
	truncate -s 0 "$INST_MNT"/swap/swapfile
	chattr +C "$INST_MNT"/swap/swapfile
	btrfs property set "$INST_MNT"/swap/swapfile compression none
	count=$(free -m | awk 'FNR == 2 {print $2}' | awk '{print $1 + int(sqrt($1))}')
	echo "$count"
	dd if=/dev/zero of="$INST_MNT"/swap/swapfile bs=1M count="$count" status=progress
	chmod 700 "$INST_MNT"/swap
	chmod 600 "$INST_MNT"/swap/swapfile
	mkswap "$INST_MNT"/swap/swapfile
	echo /swap/swapfile none swap defaults 0 0 >>"$INST_MNT"/etc/fstab
}

network() {
	yellow "network"
  # hostname
	echo "$INST_HOST" >"$INST_MNT"/etc/hostname

	# timezone
	ln -sf "$INST_TZ" "$INST_MNT"/etc/localtime
	hwclock --systohc

	# locale
	echo "en_US.UTF-8 UTF-8" >>"$INST_MNT"/etc/locale.gen
	echo "LANG=en_US.UTF-8" >>"$INST_MNT"/etc/locale.conf

	tee "$INST_MNT"/etc/systemd/network/20-default.network <<EOF
[Match]
Name=$INET

[Network]
DHCP=yes
EOF
}

chroot() {
	yellow "chroot"
	# arch-chroot (arch) artix-chroot (artix)
  user=flat
  # arch-chroot /mnt /bin/bash -c "cd /etc/apl-files && ./runme.sh"
	arch-chroot "$INST_MNT" /usr/bin/env DISK="$DISK" bash <<EOF
# apply locals
locale-gen

# enable connman
systemctl enable systemd-networkd systemd-resolved

# root pass
echo -e "arch\narch" | passwd

# initramfs
mkinitcpio -P

# btrfs service
systemctl enable grub-btrfs.path

# user
useradd -s /bin/bash -U -G wheel,video -m --btrfs-subvolume-home "$user"

# snapper
lsblk -f
umount /.snapshots/
rmdir /.snapshots/
snapper --no-dbus -c root create-config /
rmdir /.snapshots/
mkdir /.snapshots/
mount /.snapshots/
snapper --no-dbus -c home create-config /home/
snapper --no-dbus -c "$user" create-config /home/"$user"
systemctl enable /lib/systemd/system/snapper-*

# grub init
grub-install
grub-install --removable
grub-install "$DISK"
grub-mkconfig -o /boot/grub/grub.cfg

# confirm fstab
cat /etc/fstab
EOF

# finalize
mount | grep "$INST_MNT/" | tac | cut -d" " -f3 | xargs -i{} umount -lf {}
umount "$INST_MNT"
cryptsetup close "$BOOT_MAPPER_NAME"
# reboot
}

main() {
	INST_TZ=/usr/share/zoneinfo/America/Los_Angeles
	INST_HOST='earth'
	INET=ens3
	DISK=/dev/disk/by-id/ata-QEMU_HARDDISK_QM00001
	INST_MNT=$(mktemp -d)
	BOOT_PARTUUID=$(blkid -s PARTUUID -o value "$DISK-part2")
	BOOT_MAPPER_NAME="cryptroot-luks1-partuuid-$BOOT_PARTUUID"
	BOOT_MAPPER_PATH="/dev/mapper/$BOOT_MAPPER_NAME"
	# ssd needs explicit?
	BTRFS_OPTS="rw,noatime,compress=zstd,space_cache=v2,discard=async,commit=120,autodefrag"
	CRYPTKEY=/lukskey/crypto_keyfile.bin
	yellow "$DISK"
	yellow "$INST_MNT"
	yellow "$BOOT_PARTUUID"
	yellow "$BOOT_MAPPER_NAME"
	yellow "$BOOT_MAPPER_PATH"
	yellow "$BTRFS_OPTS"
	yellow "$CRYPTKEY"
	yellow "$INET"
	init
	partition
	format
	subvols
	mount_sub
	cow
	efi
	strap
	fstab
	key
	ramfs
	grub
	swap
	network
	# chroot
}
main
