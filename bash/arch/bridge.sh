#!/bin/bash

cd /etc/systemd/network || exit
br=br0
eth=enp7s0

file=br.netdev
sudo tee $file << EOF
[NetDev]
Name=$br
Kind=bridge
EOF

file=1-$br-bind.network
sudo tee $file << EOF
[Match]
Name=$eth

[Network]
Bridge=$br
EOF

file=2-$br-dhcp.network
sudo tee $file << EOF
[Match]
Name=$br

[Network]
DHCP=ipv4
EOF

sudo systemctl enable --now systemd-networkd
