#!/bin/bash

LANG="en_US.UTF-8"
KEYMAP="us"
INSTALL_PATH="/mnt/install"
PARTLABEL_BOOT="boot"
PARTLABEL_ROOT="root"
LABEL_ARCH="arch"
CRYPT_DEV_LABEL="system"
HOSTNAME="archlinux"
LUKS_PBKDF_ITERATIONS=500000

BTRFS_SYS_SUBVOLUME="@"
BTRFS_SYS_SNAPSHOTS_SUBVOLUME="@snapshots"
BTRFS_VAR_LOG_SUBVOLUME="@var/log"
BTRFS_SWAP_SUBVOLUME="@swap"
BTRFS_HOME_SUBVOLUME="@home"

MOUNT_OPTS="space_cache=v2,noatime,compress=zstd:2"

LBLUE='\033[1;34m'
RED='\033[0;31m'
GREEN='\033[0;32m'
NC='\033[0m'

check_efi() {
	[ ! -e /sys/firmware/efi/efivars ] && echo "$RED [ERROR] Not booted via UEFI, installation is not possible! $NC" && exit 1
	return 0
}

print_config() {
	echo -e "\n$LBLUE >> Config $NC"
	head -n 57 "$0" | grep -v "^#" | grep -v "^$"
	echo -e "\n"
	return 0
}

get_uuid() {
	blkid "$1" -s UUID -o value
}

select_device() {
	echo -e "\n$LBLUE >> Select Install Device $NC"
	lsblk
	echo -ne "Set device name (e.g. nvme0n1) : " && read -r device
	[ -z "$device" ] && exit 1
	if [ ! -e /dev/"$device" ]; then
		echo -e "${RED[ERROR]} Device not found! $NC"
		exit 1
	else
		device="/dev/$device"
	fi
	_DEVICE="$device"
	return 0
}

partition_drive() {
	device="$1"
	shift
	echo -e "\n$LBLUE >> Partition Drive $NC"

	[ ! -e "$device" ] && echo "Device not found!" && exit 1
	echo -n "All data on $device will be deleted, continue? (Y/n) : " && read -r delete && delete=${delete:-Yes}
	[ "$delete" != "Yes" ] && [ "$delete" != "yes" ] && [ "$delete" != "Y" ] && [ "$delete" != "y" ] && exit 1

	# if installation failed we need to remove the broken setup first
	if [ -e /dev/mapper/"$CRYPT_DEV_LABEL" ]; then
		umount -A -v -f /dev/mapper/"$CRYPT_DEV_LABEL"
		cryptsetup luksClose "$CRYPT_DEV_LABEL"
	fi

	readarray -t arr < <(find "$_DEVICE"*)
	if [ "${#arr[@]}" -gt "1" ]; then
		for i in "${arr[@]}"; do
			echo "umount $i" && umount -f "$i"
		done
	fi

	wipefs --force --quiet --all "$device" >/dev/null 2>&1

	parted --script "$device" mklabel gpt
	parted --script "$device" mkpart primary 1MiB 256MiB name 1 "$PARTLABEL_BOOT"
	parted --script "$device" set 1 boot on
	parted --script "$device" mkpart primary 256MiB 100% name 2 "$PARTLABEL_ROOT"
	return 0
}

encrypt_drive() {
	crypt_dev="$1"
	shift
	echo -e "\n$LBLUE >> Encrypt Drive $NC"

	loadkeys us
	while true; do
		echo -en "\nEnter passphrase for drive: " && read -rs drive_passphrase
		echo -en "\nVerify passphrase for drive: " && read -rs drive_passphrase_verify

		if [ "$drive_passphrase" == "$drive_passphrase_verify" ] && [ -n "$drive_passphrase" ]; then
			echo -e "\n${GREEN}OK $NC" && break
		else
			echo -e "$RED\n[ERROR] passphrase does not match! $NC"
		fi
	done
	loadkeys "$KEYMAP"

	if [ "$LUKS_PBKDF_ITERATIONS" -le "100000" ]; then
		echo "[LUKS] manual pbkdf iteration is disabled, determine automatically"
		echo -en "$drive_passphrase" | cryptsetup luksFormat --type luks1 -s 512 -h sha512 "$crypt_dev"
	else
		echo "[LUKS] use $LUKS_PBKDF_ITERATIONS pbkdf iterations"
		echo -en "$drive_passphrase" | cryptsetup luksFormat --type luks1 --pbkdf-force-iterations "$LUKS_PBKDF_ITERATIONS" -s 512 -h sha512 "$crypt_dev"
	fi
	echo -en "$drive_passphrase" | cryptsetup open --type luks1 "$crypt_dev" "$CRYPT_DEV_LABEL"
	return 0
}

create_btrfs_subvolume_recursive() {
	# Installer btrfs helper function
	btrfs_path="$1"
	shift

	IFS='/'
	read -ra str_arr <<<"$btrfs_path"
	subvol_path=""
	for subvol in "${str_arr[@]}"; do
		[ "$subvol" == "" ] && continue
		subvol_path="$subvol_path/$subvol"
		if [ ! -e "/mnt/btrfs-root$subvol_path" ]; then
			btrfs subvolume create "/mnt/btrfs-root$subvol_path"
		fi
	done
	unset IFS
}

format_filesystem() {
	boot_dev="$1"
	shift
	root_dev="$1"
	shift
	echo -e "\n$LBLUE >> Formatting Filesystem $NC"

	# formatting filesystem
	mkfs.vfat -F32 "$boot_dev"
	mkfs.btrfs -f -L "$LABEL_ARCH" "$root_dev"

	mkdir -p /mnt/btrfs-root
	mount -t btrfs -o "$MOUNT_OPTS" "$root_dev" /mnt/btrfs-root
	return 0
}

make_subvols() {
	boot_dev="$1"
	shift
	root_dev="$1"
	shift
	echo -e "\n$LBLUE >> Make Subvols $NC"

	arr_nocow=(
		# @var/games
		# @var/www
		@var
		"$BTRFS_VAR_LOG_SUBVOLUME"
		@var/log/journal
		@var/cache
		@var/crash
		@var/tmp
		@var/spool
		# @var/lib/libvirt/images
		# @var/lib/docker
		# @var/lib/machines
		# @var/lib/containers
		# @var/lib/AccountsService
		# @var/lib/NetworkManager
	)

	arr=(
		"$BTRFS_SYS_SUBVOLUME"
		"$BTRFS_HOME_SUBVOLUME"
		"$BTRFS_SWAP_SUBVOLUME"
		"$BTRFS_SYS_SNAPSHOTS_SUBVOLUME"
		@usr
		# exclude from snapshots
		@usr/local
		@/opt
		@/srv
	)

	combine=("${arr[@]}" "${arr_nocow[@]}")

	# create subvols
	for i in "${combine[@]}"; do
		if [ ! -e "/mnt/btrfs-root/$i" ]; then
			btrfs subvolume create "/mnt/btrfs-root/$i" &>/dev/null
		fi
	done

	# mount subvols
	BASIC_OPTS="space_cache=v2,noatime"
	MOUNT_OPTS="$BASIC_OPTS,compress=zstd:2"
	MOUNT_PATH="$INSTALL_PATH"
	SUBVOL="@"
	mkdir -p "$MOUNT_PATH"
	mount -t btrfs -o "$MOUNT_OPTS,subvol=$SUBVOL" "$root_dev" "$MOUNT_PATH"

	MOUNT_OPTS="$BASIC_OPTS,compress=zstd:2,nodev,nosuid"
	MOUNT_PATH="$INSTALL_PATH/home"
	SUBVOL="@home"
	mkdir -p "$MOUNT_PATH"
	mount -t btrfs -o "$MOUNT_OPTS,subvol=$SUBVOL" "$root_dev" "$MOUNT_PATH"

	MOUNT_OPTS="$BASIC_OPTS"
	MOUNT_PATH="$INSTALL_PATH/swap"
	SUBVOL="@swap"
	mkdir -p "$MOUNT_PATH"
	mount -t btrfs -o "$MOUNT_OPTS,subvol=$SUBVOL" "$root_dev" "$MOUNT_PATH"

	MOUNT_OPTS="$BASIC_OPTS,compress=zstd:2"
	MOUNT_PATH="$INSTALL_PATH/.snapshots"
	SUBVOL="@snapshots"
	mkdir -p "$MOUNT_PATH"
	mount -t btrfs -o "$MOUNT_OPTS,subvol=$SUBVOL" "$root_dev" "$MOUNT_PATH"

	MOUNT_OPTS="$BASIC_OPTS,compress=zstd:2"
	MOUNT_PATH="$INSTALL_PATH/usr"
	SUBVOL="@usr"
	mkdir -p "$MOUNT_PATH"
	mount -t btrfs -o "$MOUNT_OPTS,subvol=$SUBVOL" "$root_dev" "$MOUNT_PATH"

	MOUNT_OPTS="$BASIC_OPTS,compress=zstd:2"
	MOUNT_PATH="$INSTALL_PATH/usr/local"
	SUBVOL="@usr/local"
	mkdir -p "$MOUNT_PATH"
	mount -t btrfs -o "$MOUNT_OPTS,subvol=$SUBVOL" "$root_dev" "$MOUNT_PATH"

	MOUNT_OPTS="$BASIC_OPTS,compress=zstd:2"
	MOUNT_PATH="$INSTALL_PATH/opt"
	SUBVOL="@/opt"
	mkdir -p "$MOUNT_PATH"
	mount -t btrfs -o "$MOUNT_OPTS,subvol=$SUBVOL" "$root_dev" "$MOUNT_PATH"

	MOUNT_OPTS="$BASIC_OPTS,compress=zstd:2"
	MOUNT_PATH="$INSTALL_PATH/srv"
	SUBVOL="@/srv"
	mkdir -p "$MOUNT_PATH"
	mount -t btrfs -o "$MOUNT_OPTS,subvol=$SUBVOL" "$root_dev" "$MOUNT_PATH"

	# no cow
	MOUNT_OPTS="$BASIC_OPTS,nodatacow"
	MOUNT_PATH="$INSTALL_PATH/var"
	SUBVOL="@var"
	mkdir -p "$MOUNT_PATH"
	mount -t btrfs -o "$MOUNT_OPTS,subvol=$SUBVOL" "$root_dev" "$MOUNT_PATH"

	MOUNT_OPTS="$BASIC_OPTS,nodatacow,nodev,nosuid,noexec"
	MOUNT_PATH="$INSTALL_PATH/var/log"
	SUBVOL="@var/log"
	mkdir -p "$MOUNT_PATH"
	mount -t btrfs -o "$MOUNT_OPTS,subvol=$SUBVOL" "$root_dev" "$MOUNT_PATH"

	MOUNT_OPTS="$BASIC_OPTS,nodatacow"
	MOUNT_PATH="$INSTALL_PATH/var/log/journal"
	SUBVOL="@var/log/journal"
	mkdir -p "$MOUNT_PATH"
	mount -t btrfs -o "$MOUNT_OPTS,subvol=$SUBVOL" "$root_dev" "$MOUNT_PATH"

	MOUNT_OPTS="$BASIC_OPTS,nodatacow,nodev,nosuid,noexec"
	MOUNT_PATH="$INSTALL_PATH/var/cache"
	SUBVOL="@var/cache"
	mkdir -p "$MOUNT_PATH"
	mount -t btrfs -o "$MOUNT_OPTS,subvol=$SUBVOL" "$root_dev" "$MOUNT_PATH"

	MOUNT_OPTS="$BASIC_OPTS,nodatacow,nodev,nosuid,noexec"
	MOUNT_PATH="$INSTALL_PATH/var/crash"
	SUBVOL="@var/crash"
	mkdir -p "$MOUNT_PATH"
	mount -t btrfs -o "$MOUNT_OPTS,subvol=$SUBVOL" "$root_dev" "$MOUNT_PATH"

	MOUNT_OPTS="$BASIC_OPTS,nodatacow,nodev,nosuid"
	MOUNT_PATH="$INSTALL_PATH/var/tmp"
	SUBVOL="@var/tmp"
	mkdir -p "$MOUNT_PATH"
	mount -t btrfs -o "$MOUNT_OPTS,subvol=$SUBVOL" "$root_dev" "$MOUNT_PATH"

	MOUNT_OPTS="$BASIC_OPTS,nodatacow,nodev,nosuid,noexec"
	MOUNT_PATH="$INSTALL_PATH/var/spool"
	SUBVOL="@var/spool"
	mkdir -p "$MOUNT_PATH"
	mount -t btrfs -o "$MOUNT_OPTS,subvol=$SUBVOL" "$root_dev" "$MOUNT_PATH"

	# MOUNT_OPTS="$BASIC_OPTS,nodatacow,nodev,nosuid,noexec"
	# MOUNT_PATH="$INSTALL_PATH/var/lib/libvirt/images"
	# SUBVOL="@var/lib/libvirt/images"
	# mkdir -p "$MOUNT_PATH"
	# mount -t btrfs -o "$MOUNT_OPTS,subvol=$SUBVOL" "$root_dev" "$MOUNT_PATH"
	#
	# MOUNT_OPTS="$BASIC_OPTS,nodatacow,nodev,nosuid,noexec"
	# MOUNT_PATH="$INSTALL_PATH/var/lib/AccountsService"
	# SUBVOL="@var/lib/AccountsService"
	# mkdir -p "$MOUNT_PATH"
	# mount -t btrfs -o "$MOUNT_OPTS,subvol=$SUBVOL" "$root_dev" "$MOUNT_PATH"

	mkdir -p "$INSTALL_PATH"/boot/efi
	mount -t vfat -o "nodev,nosuid,noexec" "$boot_dev" "$INSTALL_PATH"/boot/efi

	# nocow subvols
	for i in "${arr_nocow[@]}"; do
		chattr +C "/mnt/btrfs-root/$i"
	done

	# for i in "${combine[@]}"; do
	# 	MPATH="$INSTALL_PATH$(echo "$i" | sed "s/@/\//g")"
	# 	mkdir -P "$MPATH"
	# 	mount -t btrfs -o "$MOUNT_OPTS,subvol=$i" "$root_dev" "$MPATH"
	# done

	# Create fstab
	mkdir -p "$INSTALL_PATH"/etc
	echo -e "# <uuid>  <path>   <fs>    <options>  <0>  <order>" >"$INSTALL_PATH"/etc/fstab

	btrfs_dev_uuid=$(get_uuid "$root_dev")
	boot_dev_uuid=$(get_uuid "$boot_dev")
	tee "$INSTALL_PATH"/etc/fstab <<-EOF
		  UUID=$btrfs_dev_uuid / btrfs $BASIC_OPTS,compress=zstd:2,nodev,nosuid,subvol=$BTRFS_SYS_SUBVOLUME 0 0
		  UUID=$btrfs_dev_uuid /home btrfs $BASIC_OPTS,compress=zstd:2,nodev,nosuid,subvol=$BTRFS_HOME_SUBVOLUME 0 2
		  UUID=$btrfs_dev_uuid /swap btrfs $BASIC_OPTS,compress=zstd:2,nodev,nosuid,subvol=$BTRFS_SWAP_SUBVOLUME 0 2
		  UUID=$btrfs_dev_uuid /.snapshots btrfs $BASIC_OPTS,compress=zstd:2,subvol=@snapshots 0 2
		  UUID=$btrfs_dev_uuid /usr btrfs $BASIC_OPTS,compress=zstd:2,subvol=@usr 0 2
		  UUID=$btrfs_dev_uuid /usr/local btrfs $BASIC_OPTS,compress=zstd:2,subvol=@usr/local 0 2
		  UUID=$btrfs_dev_uuid /opt btrfs $BASIC_OPTS,compress=zstd:2,subvol=@/opt 0 2
		  UUID=$btrfs_dev_uuid /srv btrfs $BASIC_OPTS,compress=zstd:2,subvol=@/srv 0 2
		  UUID=$btrfs_dev_uuid /var btrfs $BASIC_OPTS,nodatacow,subvol=@var 0 2
		  UUID=$btrfs_dev_uuid /var/log btrfs $BASIC_OPTS,nodatacow,nodev,nosuid,noexec,subvol=$BTRFS_VAR_LOG_SUBVOLUME 0 2
		  UUID=$btrfs_dev_uuid /var/log/journal btrfs $BASIC_OPTS,nodatacow,subvol=@var/log/journal 0 2
		  UUID=$btrfs_dev_uuid /var/cache btrfs $BASIC_OPTS,nodatacow,nodev,nosuid,noexec,subvol=@var/cache 0 2
		  UUID=$btrfs_dev_uuid /var/crash btrfs $BASIC_OPTS,nodatacow,nodev,nosuid,noexec,subvol=@var/crash 0 2
		  UUID=$btrfs_dev_uuid /var/tmp btrfs $BASIC_OPTS,nodatacow,nodev,nosuid,subvol=@var/tmp 0 2
		  UUID=$btrfs_dev_uuid /var/spool btrfs $BASIC_OPTS,nodatacow,nodev,nosuid,noexec,subvol=@var/spool 0 2
		  UUID=$btrfs_dev_uuid /.snapshots btrfs $BTRFS_OPTS,subvol=$BTRFS_SYS_SNAPSHOTS_SUBVOLUME 0 2
		  UUID=$btrfs_dev_uuid /swap btrfs defaults,ssd,noatime,subvol=$BTRFS_SWAP_SUBVOLUME 0 2
		  UUID=$boot_dev_uuid /boot/efi vfat defaults,nodev,nosuid,noexec 0 2
	EOF
	return 0
}

# MAIN
check_efi
print_config
select_device # Set _DEVICE
partition_drive "$_DEVICE"

_CRYPT_DEV="/dev/disk/by-partlabel/$PARTLABEL_ROOT"
encrypt_drive "$_CRYPT_DEV" # Set _DRIVE_PASSPHRASE

_BOOT_DEV="/dev/disk/by-partlabel/$PARTLABEL_BOOT"
_ROOT_DEV="/dev/mapper/$CRYPT_DEV_LABEL"
format_filesystem "$_BOOT_DEV" "$_ROOT_DEV"
make_subvols "$_BOOT_DEV" "$_ROOT_DEV"
