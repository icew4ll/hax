#!/bin/bash

url=https://raw.githubusercontent.com/niki-on-github/Arch-Linux-Installation-Script/master/install.sh
file=niki.sh
curl -SL $url > $file
chmod +x $file
bash $file

PS3='Please select the kernel: '
options=("linux" "linux-lts" "linux-zen" "linux-hardened")
select opt in "${options[@]}"; do
	INST_LINVAR=$opt
	break
done

PS3='Please select the disk to install: '
#(lsblk -d | tail -n+2 | awk '{print $1" "$4}')
#$(lsblk -d | tail -n+2 | cut -d" " -f1)
disks=("$(ls -d /dev/disk/by-id/* | grep -v part)")
DISK=""
select opt in "${disks[@]}"; do
	#DISK=/dev/$opt
	DISK=$opt
	break
done

print_logo() {
	clear
	echo -e '\e[H\e[2J'
	echo -e '          \e[0;36m.'
	echo -e '         \e[0;36m/ \'
	echo -e '        \e[0;36m/   \      \e[1;37m               #     \e[1;36m| *'
	echo -e '       \e[0;36m/^.   \     \e[1;37m a##e #%" a#"e 6##%  \e[1;36m| | |-^-. |   | \ /'
	echo -e '      \e[0;36m/  .-.  \    \e[1;37m.oOo# #   #    #  #  \e[1;36m| | |   | |   |  X'
	echo -e '     \e[0;36m/  (   ) _\   \e[1;37m%OoO# #   %#e" #  #  \e[1;36m| | |   | ^._.| / \ \e[0;37mTM'
	echo -e '    \e[1;36m/ _.~   ~._^\'
	echo -e '   \e[1;36m/.^         ^.\ \e[0;37mTM'
	echo -e " "
	return 0
}
