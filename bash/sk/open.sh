#!/bin/bash

file=$(fd -t f 2>/dev/null | sk)
if [[ "$file" != "" ]]; then
	# "$EDITOR" "$file"
	case "$(file -biL "$file")" in
	*text*)
		"$EDITOR" "$file"
		;;
	*)
		xdg-open "$file" >/dev/null 2>&1
		;;
	esac
fi

cd $"($env.HOME)/m/vim" ; sh ~/.config/nnn/plugins/open
