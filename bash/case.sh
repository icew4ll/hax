#!/bin/bash

KERNEL=linux-zen
case "$KERNEL" in
linux-hardened)
	base_packages+=(linux-hardened linux-hardened-headers)
	;;
linux-zen)
	base_packages+=(linux-zen linux-zen-headers)
	;;
*)
	echo invalid
	;;
esac
echo "${base_packages[@]}"
