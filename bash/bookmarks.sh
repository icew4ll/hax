#!/bin/bash

if [ -z "$BOOKMARKS_DIR" ]; then
    BOOKMARKS_DIR="${XDG_CACHE_HOME:-$HOME/.cache}/nnn/bookmarks"
fi

# Check if NNN_PIPE is set
if [ -z "$NNN_PIPE" ]; then
    echo 'ERROR: NNN_PIPE is not set' | ${PAGER:-less}
    exit 2
fi

# Get all directory symlinks
get_links() {
    for entry in "$1"/*; do

        # Skip unless directory symlink
        [ -h "$entry" ] || continue
        [ -d "$entry" ] || continue

        printf "%10s -> %s\n" "$(basename "$entry")" "$(readlink -f "$entry")"
    done | sk |
    awk 'END {
        if (length($1) == 0) { print "'"$PWD"'" }
        else { print "'"$BOOKMARKS_DIR"'/"$1 }
    }'
}

# Choose symlink with fzf
cddir="$(get_links "$BOOKMARKS_DIR")"

# Writing result to NNN_PIPE will change nnn's active directory
# https://github.com/jarun/nnn/tree/master/plugins#send-data-to-nnn
context=0
printf "%s" "${context}c$(readlink -f "$cddir")" > "$NNN_PIPE"
