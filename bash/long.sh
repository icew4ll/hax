#!/bin/bash

ip=172.104.114.203

expect <<EOF
set prompt {[#|%|>|$] $}
spawn ssh $UI@216.230.241.161
expect "assword"
send "$PG\n"
expect "\$"
send "telnet 10.254.0.54\n"
expect "assword"
send "1FdQAzBC\n"
expect \$prompt
send "en\n\n"
expect \$prompt
send "conf t\n"
expect \$prompt
send "object-group network blockothers\n"
expect \$prompt
send "network-object host $ip\n"
expect \$prompt
send "write\n"
expect \$prompt
send "exit\n"
expect \$prompt
send "exit\n"
expect \$prompt
send "exit\n"
expect \$prompt
send "exit\n"
expect eof
EOF
