#!/bin/bash

# base
sudo xbps-install -Sy git

# dwm
sudo xbps-install -Sy base-devel libX11-devel libXft-devel libXinerama-devel font-symbola
