#!/bin/bash

source utils.sh

url=https://github.com/void-linux/void-packages
base=$(basename $url)
dir="$proj"/"$base"

clean_dir "$dir"
git clone "$url" "$dir"
ls "$dir"
