#!/bin/bash

dev=/dev/sda

status() {
	lsblk -f
}

green() {
	echo -e "\e[0;32m$1\e[0m"
}

partition() {
	# write partitions
	wipefs -a $dev
	printf "g\nn\n1\n\n+260M\nt\n1\nn\n\n\n\nw\n" | fdisk "$dev"
}

format() {
	# efi
	mkfs.fat $dev"1"

	# format
	mkfs.ext4 $dev"2"

}

mounts() {
	mount $dev"2" /mnt
  mkdir -p /mnt/boot/efi
	mount -o rw,noatime $dev"1" /mnt/boot/efi
}

swap() {
	swap=/mnt/var/swap/swapfile
	# Create a zero length file that will serve as the swapfile
	truncate -s 0 $swap

	# Set permissions for the swap file
	chmod 600 $swap

	# Create the swap file
	count=$(free -m | awk 'FNR == 2 {print $2}' | awk '{print $1 + int(sqrt($1))}')
	echo "$count"
	dd if=/dev/zero of=$swap bs=1M count="$count" status=progress conv=fdatasync

	# Format the swap file
	mkswap "$swap"
}

channels() {
	file=~/.config/guix/base-channels.scm
	mkdir -p "$(dirname $file)"
	tee $file <<EOF
(cons* (channel
        (name 'nvidia)
        (url "https://gitlab.com/squarerectangle/nvidiachannel"))
       (channel
        (name 'nonguix)
        (url "https://gitlab.com/nonguix/nonguix")
        ;; Enable signature verification:
        (introduction
         (make-channel-introduction
          "897c1a470da759236cc11798f4e0a5f7d4d59fbc"
          (openpgp-fingerprint
           "2A39 3FFF 68F4 EF7A 3D29  12AF 6F51 20A0 22FB B2D5"))))
       %default-channels)
EOF
	guix pull --channels=$file
	file=~/.config/guix/channels.scm
	guix describe --format=channels >$file
  hash guix
  green "$(guix describe)"
}

config() {
	efi=$(lsblk -o PATH,UUID | grep -E "(sda1)" | awk '{print $2}')
	root=$(lsblk -o PATH,UUID | grep -E "(sda2)" | awk '{print $2}')
	ssh="ssh-rsa AAAAB3NzaC1yc2EAAAADAQABAAACAQCqfZkR3Pk6Wvp/T//XhnUWHNh75ZdQzrlAHrvd/bjC3lAHVv7uqj1g8ajpco/0WwSDZXcb2CAyIwBwepAW2tXuFL2I6xDQJHDAcBF28IZM0QXGJ2bqtVHfz7MiwisYczDj9RzuWNVkg/aOeLTM3k5tu0IIcjhRnuZu8xozuEzRTL7gnKM3+mS1Fe91pkD6VA8HFOjQ2Hk/v5baVqQAL+NzRBHz+6/JKeDY2d/xWVJHluQL/MOYCZIoDGJaBqjI5jLHhFykFJeXixXBJ4+kzHuqAznug6W3zg2Fo2/Om2oAcnuhzn0dic2p60BaB0YR0UgULXnVePZu4b2cb4J5kLkABvlHvSPJQZrXa7zUAJYgdmlnH/R/W9EDI8aA6JwuppjKtbiDxjOdUs7CXy2JjnkxaXDcYb7484RwX+mkY+vgOTy6i3pGwXQ9fj750g5PbmJQs9rbIBlFg82fr8Z+iCdO2ICcFvErwVYgl97uPTmKtfZ2AS4wT9y4+qKBt62i4wSZUJTxIS1/6NaH1HtePHWj+5/3w6PU53swY1/1t88O3Zt7R0gKW1nzTKY8WBRj08h/9MGMeNRkFfss28ytd5H1jysrKMI/2zvyxzFcJiTBCuCKPVIVeOktzArUnC7OtOa6lhIBOU7DGpqt6E3/7IZDa03AVIFBJPOKtIs1ExeicQ== ice@wall"
	file=test.scm
	tee $file <<EOF
(use-modules
 (gnu)
 (gnu system nss)
 (nongnu packages linux)
 (nongnu system linux-initrd)
 )
(use-service-modules
 desktop
 networking
 ssh
 xorg)

(define %root-ssh-public-key
  "$ssh")

(operating-system
 ;; kernel
 (kernel linux)
 (initrd microcode-initrd)
 (firmware (list linux-firmware))
 (kernel-arguments '("quiet" "modprobe.blacklist=radeon" "net.ifnames=0"))

 ;; sys
 (locale "en_US.utf8")
 (timezone "America/Los_Angeles")
 (keyboard-layout (keyboard-layout "us"))
 (host-name "earth")
 (users (cons* (user-account
                (name "flat")
                (comment "Flat")
                (group "users")
                (home-directory "/home/flat")
                (supplementary-groups
                 '("wheel" "netdev" "audio" "video")))
               %base-user-accounts))

 (packages (append
            (map specification->package+output
                 ;; System packages
                 '("vim" "awesome" "kitty"
                   "curl" "wget" "git"
                   "tmux" "perl" "nnn" "rsync"
                   "nss-certs"))
            %base-packages))

 (services
  (append
   (list (service openssh-service-type
                  (openssh-configuration
                   (permit-root-login 'prohibit-password)
                   (password-authentication? #f)
                   (authorized-keys
                    \`(("root" ,(plain-file "authorized_keys"
                                           %root-ssh-public-key))))))
         (set-xorg-configuration
          (xorg-configuration
           (keyboard-layout keyboard-layout))))
   %desktop-services))
 (bootloader
  (bootloader-configuration
   (bootloader grub-efi-bootloader)
   (targets (list "/boot/efi"))
   (keyboard-layout keyboard-layout)))
 (file-systems
  (cons* (file-system
          (mount-point "/boot/efi")
          (device (uuid "$efi" 'fat16))
          (type "vfat"))
         (file-system
          (mount-point "/")
          (device
           (uuid "$root"
                 'ext4))
          (type "ext4"))
         %base-file-systems)))
EOF
	herd start cow-store /mnt
	guix system init $file /mnt
	# ECHO REBOOTING NOW
	# reboot
}

main() {
	# sanity
	partition
	format
  mounts
	swap
	status
	channels
	config
}
main
