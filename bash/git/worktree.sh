#!/bin/bash
# init tmp
tmp=$(mktemp -d)
bare=".bare"
br=main
cd "$tmp" || exit
pwd

# init bare
git init --initial-branch="$br" --bare "$bare"
# echo "gitdir: ./$bare" >.git

# clone bare
git clone "$bare" "$br"
echo "$tmp"/"$br" | xclip -selection c

# init new folder
cd "$br" || exit
echo t1 >t1
git add .
git commit -m "init $br"
git push origin "$br"

# clean
# cd || exit
# rm -rf "$tmp"
